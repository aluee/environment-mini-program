import qqmapwx from '../../components/qqmap-wx-jssdk.min.js';
var chooseLocation = require('../../components/qqmap-wx-jssdk.js');
var qqmapsdk = new chooseLocation({
    key:'PQVBZ-R4DCJ-P6QF6-X4ALH-U7VS6-LCBDO'
});
let that = null;
Page({
    data:{

      //地图变量
      address: "",
      locationName: "",

    },

  //   onShow: function () {
  //     // 从地图选点插件返回后，在页面的onShow生命周期函数中能够调用插件接口，取得选点结果对象
  //     // 如果点击确认选点按钮，则返回选点结果对象，否则返回null
  //     const location = chooseLocation.getLocation();
  //     if(location){
  //         this.setData({
  //             address: location.address?location.address : "",
  //             locationName: location.name?location.name : ""
  //         });
  //     }

  // },

  //显示地图
  // showMap() {
  //     //使用在腾讯位置服务申请的key（必填）
  //     const key = "PQVBZ-R4DCJ-P6QF6-X4ALH-U7VS6-LCBDO"; 
  //     //调用插件的app的名称（必填）
  //     const referer = "腾讯位置服务地图选点";//STM32 environment 
  //     wx.navigateTo({
  //         url: 'plugin://chooseLocation/index?key=' + key + '&referer=' + referer
  //     });
  // },

  formSubmit(e) {
    var _this = this;
    console.log(getApp().globalData.location);
    qqmapsdk.reverseGeocoder({
      //位置坐标，默认获取当前位置，非必须参数
      /**
       * 
       //Object格式
        location: {
          latitude: 39.984060,
          longitude: 116.307520
        },
      */
      /**
       *
       //String格式
        location: '39.984060,116.307520',
      */
      // location: e.detail.value.reverseGeo || '', //获取表单传入的位置坐标,不填默认当前位置,示例为string格式
      location: getApp().globalData.location,
      
      //get_poi: 1, //是否返回周边POI列表：1.返回；0不返回(默认),非必须参数
      success: function(res) {//成功后的回调
        console.log(res);
        var res = res.result;
        var mks = [];
        /**
         *  当get_poi为1时，检索当前位置或者location周边poi数据并在地图显示，可根据需求是否使用
         *
            for (var i = 0; i < result.pois.length; i++) {
            mks.push({ // 获取返回结果，放到mks数组中
                title: result.pois[i].title,
                id: result.pois[i].id,
                latitude: result.pois[i].location.lat,
                longitude: result.pois[i].location.lng,
                iconPath: './resources/placeholder.png', //图标路径
                width: 20,
                height: 20
            })
            }
        *
        **/
        //当get_poi为0时或者为不填默认值时，检索目标位置，按需使用
        mks.push({ // 获取返回结果，放到mks数组中
          title: res.address,
          id: 0,
          latitude: res.location.lat,
          longitude: res.location.lng,
          iconPath: './resources/placeholder.png',//图标路径
          width: 20,
          height: 20,
          callout: { //在markers上展示地址名称，根据需求是否需要
            content: res.address,
            color: '#000',
            display: 'ALWAYS'
          }
        });
        _this.setData({ //设置markers属性和地图位置poi，将结果在地图展示
          markers: mks,
          poi: {
            latitude: res.location.lat,
            longitude: res.location.lng
          }
        });
      },
      fail: function(error) {
        console.error(error);
      },
      complete: function(res) {
        console.log(res);
      }
    })
}
})