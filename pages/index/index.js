import mqtt from'../../utils/mqtt.js';
const aliyunOpt = require('../../utils/aliyun/aliyun_connect.js');
let that = null;
var alcohol,temperature,CO,CO2,latitude,longitude,temperature_ceilling,temperature_floor,alcohol_ceilling,CO_ceilling,CO2_ceilling;
Page({
    data:{

      //设置温度值和湿度值 
      temperature:"0",
      humidity:"0",
      alcohol:"0",
      CO:"0",
      CO2:"0",
      latitude:"0",
      longitude:"0",
      temperature_ceilling:"0",
      temperature_floor:"0",
      alcohol_ceilling:"0",
      CO_ceilling:"0",
      CO2_ceilling:"0",
      icon_flag:"waiting",

      client:null,//记录重连的次数
      reconnectCounts:0,//MQTT连接的配置
      options:{
        protocolVersion: 4, //MQTT连接协议版本
        clean: false,
        reconnectPeriod: 1000, //1000毫秒，两次重新连接之间的间隔
        connectTimeout: 30 * 1000, //1000毫秒，两次重新连接之间的间隔
        resubscribe: true, //如果连接断开并重新连接，则会再次自动订阅已订阅的主题（默认true）
        clientId: '',
        password: '',
        username: '',
      },

      aliyunInfo: {
        productKey: 'k0heeEqrGRi', //阿里云连接的三元组 ，请自己替代为自己的产品信息!!
        deviceName: 'wechat', //阿里云连接的三元组 ，请自己替代为自己的产品信息!!
        // deviceName: 'mqtt_test', //阿里云连接的三元组 ，请自己替代为自己的产品信息!!
        deviceSecret: '7b4df9a14e5a6b8b787f145c16999299', //阿里云连接的三元组 ，请自己替代为自己的产品信息!!
        // deviceSecret: '99888d2f7fcd0a541b16f874c749666f', //阿里云连接的三元组
        regionId: 'cn-shanghai', //阿里云连接的三元组 ，请自己替代为自己的产品信息!!
        pubTopic: '/k0heeEqrGRi/wechat/user/data', //发布消息的主题 
        subTopic: '/k0heeEqrGRi/wechat/user/data', //订阅消息的主题/sys/k0heeEqrGRi/mqtt_test/thing/service/property/set
        // subTopic: '/sys/k0heeEqrGRi/mqtt_test/thing/service/property/set', //订阅消息的主题
      },
    },
    

  onLoad:function(){
    that = this;
    let clientOpt = aliyunOpt.getAliyunIotMqttClient({
      productKey: that.data.aliyunInfo.productKey,
      deviceName: that.data.aliyunInfo.deviceName,
      deviceSecret: that.data.aliyunInfo.deviceSecret,
      regionId: that.data.aliyunInfo.regionId,
      port: that.data.aliyunInfo.port,
    });

    console.log("get data:" + JSON.stringify(clientOpt));
    let host = 'wxs://' + clientOpt.host;
    
    this.setData({
      'options.clientId': clientOpt.clientId,
      'options.password': clientOpt.password,
      'options.username': clientOpt.username,
    })
    console.log("this.data.options host:" + host);
    console.log("this.data.options data:" + JSON.stringify(this.data.options));

    //访问服务器
    this.data.client = mqtt.connect(host, this.data.options);

    this.data.client.on('connect', function (connack) {
      wx.showToast({
        title: '连接成功'
      })
      console.log("连接成功");
      that.setData({icon_flag:"success"});
      wx.showToast({
        title: '连接阿里云成功',
        icon: 'success',
        duration: 2000//持续的时间
      })
    })

    //接收消息监听
    this.data.client.on("message", function (topic, payload) {
      console.log(" 收到 topic:" + topic + " , payload :" + payload);
      that.setData({
        //转换成JSON格式的数据进行读取
        temperature:JSON.parse(payload).items.tp.value,
        // humidity:JSON.parse(payload).read,
        alcohol:JSON.parse(payload).items.al.value,
        CO:JSON.parse(payload).items.CO.value,
        CO2:JSON.parse(payload).items.CO2.value,
        latitude:JSON.parse(payload).items.la.value,
        longitude:JSON.parse(payload).items.lg.value,
        CO_ceilling:JSON.parse(payload).items.cc.value,
        CO2_ceilling:JSON.parse(payload).items.c2c.value,
        temperature_ceilling:JSON.parse(payload).items.tc.value,
        temperature_floor:JSON.parse(payload).items.tf.value,
        alcohol_ceilling:JSON.parse(payload).items.ac.value
      })
        temperature=JSON.parse(payload).items.tp.value;
        alcohol=JSON.parse(payload).items.al.value;
        CO=JSON.parse(payload).items.CO.value;
        CO2=JSON.parse(payload).items.CO2.value;
        CO_ceilling=JSON.parse(payload).items.cc.value;
        CO2_ceilling=JSON.parse(payload).items.c2c.value;
        temperature_ceilling=JSON.parse(payload).items.tc.value;
        temperature_floor=JSON.parse(payload).items.tf.value;
        alcohol_ceilling=JSON.parse(payload).items.ac.value;
        
        latitude=JSON.parse(payload).items.la.value;
        longitude=JSON.parse(payload).items.lg.value;

      // console.log("test: " + temperature_floor);
      getApp().globalData.latitude = Number(latitude);
      getApp().globalData.longitude = Number(longitude);
      getApp().globalData.location = latitude + ',' + longitude;//!!!!!!
      // getApp().globalData.location = '34.626544' + ',' + '113.691375';
      // console.log(getApp().globalData.location);

/*       wx.showModal({
        content: " 收到topic:[" + topic + "], payload :[" + payload + "]",
        showCancel: false,
      }); */
      if(1){
          if(temperature > temperature_ceilling || temperature < temperature_floor){
            wx.showToast({
            title: '温度异常',
            image: '../photo/温度异常.png',
            duration: 2000//持续的时间
          })
        }
        if(alcohol > alcohol_ceilling){
          if(alcohol<80){
            wx.showToast({
            title: '饮酒驾驶',
            image: '../photo/饮酒驾驶.png',
            duration: 2000//持续的时间
          })
          }
          if(alcohol>80){
            wx.showToast({
              title: '醉酒驾驶！',
              image: '../photo/醉酒驾驶.png',
              duration: 2000//持续的时间
            })
          }
        }
        if(CO > CO_ceilling){
            wx.showToast({
            title: '一氧化碳浓度异常',
            image: '../photo/一氧化碳.png',
            duration: 2000//持续的时间
          })
        }
        if(CO2 > CO2_ceilling){
            wx.showToast({
            title: '二氧化碳浓度异常',
            image: '../photo/二氧化碳.png',
            duration: 2000//持续的时间
          })
        }
      }
      
    })
    // getApp().globalData.location = this.data.latitude + ',' + this.data.longitude;//!!!!!!!!!
    //服务器连接异常的回调
    that.data.client.on("error", function (error) {
      console.log(" 服务器 error 的回调" + error)
      that.setData({icon_flag:"warn"});
    })
    //服务器重连连接异常的回调
    that.data.client.on("reconnect", function () {
      console.log(" 服务器 reconnect的回调")
      that.setData({icon_flag:"warn"});
    })
    //服务器连接异常的回调
    that.data.client.on("offline", function (errr) {
      console.log(" 服务器offline的回调")
      that.setData({icon_flag:"warn"});
    })
  },
  
  onClickOpen() {
    that.sendCommond('set', 1);
  },
  onClickOff() {
    that.sendCommond('set', 0);
  },
  sendCommond(cmd, data) {
    let sendData = {
      cmd: cmd,
      data: data,
    };

//此函数是订阅的函数，因为放在访问服务器的函数后面没法成功订阅topic，因此把他放在这个确保订阅topic的时候已成功连接服务器
//订阅消息函数，订阅一次即可 如果云端没有订阅的话，需要取消注释，等待成功连接服务器之后，在随便点击（开灯）或（关灯）就可以订阅函数
     this.data.client.subscribe(this.data.aliyunInfo.subTopic,function(err){
      if(!err){
        console.log("订阅成功");
      };
      wx.showModal({
        content: "订阅成功",
        showCancel: false,
      })
    })  
    

    //发布消息
    if (this.data.client && this.data.client.connected) {
      this.data.client.publish(this.data.aliyunInfo.pubTopic, JSON.stringify(sendData));
      console.log("************************")
      console.log(this.data.aliyunInfo.pubTopic)
      console.log(JSON.stringify(sendData))
    } else {
      wx.showToast({
        title: '请先连接服务器',
        icon: 'none',
        duration: 2000
      })
    }
  },

})